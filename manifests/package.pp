class gcp_cloudsql_proxy::package {

  # Install the cloudsql-proxy package. This package contains the binary
  # cloud_sql_proxy packaged here at Stanford to make it more convenient
  # to install. It also contains a systemd service to manage the Cloud SQL
  # Proxy server. See also
  # https://code.stanford.edu/debian-public/cloudsql-proxy
  package { 'cloudsql-proxy':
    ensure => installed
  }

}
