# 1. Download the package
# 2. Downloads the service accounts credentials file (JSON)
# 3. Creates the file /etc/cloudsql-proxy/options

define gcp_cloudsql_proxy(
  Enum['present', 'absent'] $ensure                          = 'present',
  Optional[String]          $service_credentials_wallet_name = undef,
  #
  Array[String]             $instances                       = [],
) {

  include gcp_cloudsql_proxy::package

  file { '/etc/cloudsql-proxy':
    ensure  => directory,
  }

  if ($service_credentials_wallet_name) {
    base::wallet { $service_credentials_wallet_name:
      ensure  => $ensure,
      path    => '/etc/cloudsql-proxy/cloudsql.json',
      type    => 'file',
      owner   => root,
      group   => root,
      mode    => '0640',
    }
  }

  # Make sure the cloudsql-proxy service is running.
  service { 'cloudsql-proxy':
    ensure  => running,
    require => Package['cloudsql-proxy'],
  }

  # Configure the options file. If the options file changes, restart the
  # service.
  file { '/etc/cloudsql-proxy/options':
    ensure  => $ensure,
    content => template('gcp_cloudsql_proxy/etc/cloudsql-proxy/options.erb'),
    notify  => Service['cloudsql-proxy'],
  }

  file { '/etc/filter-syslog/cloudsql-proxy':
    ensure => $ensure,
    source => 'puppet:///modules/gcp_cloudsql_proxy/etc/filter-syslog/cloudsql-proxy',
  }

}
