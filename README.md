[[_TOC_]]
# GCP Cloud SQL Proxy Puppet module

This module installs the GCP Cloud SQL Proxy software and configures its
options. This module is a "define" in case you need to install more than
one instance of the Cloud SQL Proxy service. For more information on
configuration, see the [Cloud SQL Proxy Github
page](https://github.com/GoogleCloudPlatform/cloudsql-proxy).

## Examples
The parameter `instances` is the only one required. It is an array (this allows
multiple instances to be configured):
```
gcp_cloudsql_proxy { 'example1':
  instances => ["my-gcp-project:us-west1:mysql-1=tcp:3306"],
}
```

If you have put the service account JSON credentials in a wallet object,
indicate this by using the `service_credentials_wallet_name` parameter:
```
gcp_cloudsql_proxy { 'example1':
  instances => ["my-gcp-project:us-west1:mysql-1=tcp:3306"],
  service_credentials_wallet_name
            => 'config/abcd/gcp-service-account/cloud-sql-proxy-1',
}

```
